(use-modules (haunt asset)
	     (haunt site)
	     (haunt builder blog)
	     (haunt builder atom)
	     (haunt reader skribe))

(site #:title "My First Haunt Site"
      #:domain "peanutbutterandcrackers.gitlab.io/hello-haunt"
      #:build-directory "public"
      #:default-metadata
      '((author . "Eva Luator")
	(email . "eva@example.com"))
      #:readers (list skribe-reader)
      #:builders (list (blog #:prefix "hello-haunt")
		       (atom-feed)
		       (atom-feeds-by-tag)))
